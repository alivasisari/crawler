import asyncio
import time
import aiohttp
from bs4 import BeautifulSoup
from typing import Dict
import json

class Crawler:
    url = "https://okala.com/"
    backend_url = "http://127.0.0.1:8000/okala/products/add/"

    async def download(self, category: str) -> Dict:
        # Send post request to url
        # Get html content
        print(f"Begin downloading {self.url}{category['Category']}")
        async with aiohttp.ClientSession() as session:
            async with session.get(url=f"{self.url}{category['Category']}") as response:
                if response.status == 200:
                    content = await response.text()
                else:
                    raise ValueError('Error')
                print(f"Finished downloading {self.url}{category['Category']}")
                return content

    async def send_data(self, params: dict) -> None:
        # Get parsed data
        # Send data to database
        print(f"Begin sending {self.backend_url}")
        async with aiohttp.ClientSession() as session:
            async with session.request(method='POST', url=self.backend_url, json=params) as response:
                if response.status == 200:
                    print("Successful")
                else:
                    raise ValueError('Error')
                print(f"Finished sending {self.backend_url}")

    async def web_scrape_task(self, params: dict) -> None:
        # Download json content
        content = await self.download(params)
        # Parse data
        parsed_data = await self.parser(content)
        # Send data to database
        await self.send_data(parsed_data)

    async def parser(self, html: str) -> Dict:
        # Parse data to own form
        parsed_html = BeautifulSoup(html)
        html_parse_script = parsed_html.find("script" ,attrs={"id" : "__NEXT_DATA__"}).text

        products_parsed_full_data = json.loads(html_parse_script)["props"]["pageProps"]["data"]["getListOfProduct"]["data"]["entities"]
        # Extract product data
        products = []
        for product in products_parsed_full_data:
            products.append({"name" : product["name"] , "description" : product["description"] ,"storeName" : product["storeName"],
                             "okPrice" : product["okPrice"] , "thumbImage":product["thumbImage"] , "caption" : product["caption"]})
        return {"Products" : products}

    async def run(self) -> None:
        # Run the crawling
        categories = [
            {"Category": "drinks-herbaltea/"},
            {"Category": "foodstuffs/"},
            {"Category": "office-stationery/"}
        ]
        # Add task of CategoryId
        tasks = []
        for params in categories:
            tasks.append(self.web_scrape_task(params))
        await asyncio.wait(tasks)

