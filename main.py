from okala.crawler import Crawler as okala_crawler
from tezolmarket_crawler.crawler import Crawler as tezolmarket_crawler
import asyncio

# Run the crawlers
# Download and send data

async def main() -> None:
    okala_task = asyncio.create_task(okala_crawler().run())
    tezol_task = asyncio.create_task(tezolmarket_crawler().run())
    await okala_task
    await tezol_task


if __name__ == '__main__':
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    asyncio.run(main())
