import asyncio
import time
import aiohttp
from bs4 import BeautifulSoup
from typing import Dict


class Crawler:
    url = "https://www.tezolmarket.com/Home/GetProductQueryResult"
    backend_url = "http://127.0.0.1:8000/tezolmarket/products/add/"

    async def download(self, params: dict) -> Dict:
        # send post request to url
        print(f"Begin downloading {self.url} {params['CategoryId']}")
        async with aiohttp.ClientSession() as session:
            async with session.request(method='POST', url=self.url, params=params) as response:
                if response.status == 200:
                    content = await response.json()
                else:
                    raise ValueError('Error')
                print(f"Finished downloading {self.url} {params['CategoryId']}")
                return content

    async def send_data(self, params: dict) -> None:
        # Get parsed data
        # Send data to database
        print(f"Begin sending {self.backend_url}")
        async with aiohttp.ClientSession() as session:
            async with session.request(method='POST', url=self.backend_url, json=params) as response:
                if response.status == 200:
                    print("Successful")
                else:
                    raise ValueError('Error')
                print(f"Finished sending {self.backend_url}")

    async def web_scrape_task(self, params: dict) -> None:
        # Download json content
        content = await self.download(params)
        # Parse data
        parsed_data = await self.parser(content)
        # Send data to database
        await self.send_data(parsed_data)

    async def parser(self, content: dict) -> Dict:
        # Parse data to own form
        products = list()
        for product in content["Products"]:
            products.append({"Subtitle": product['Subtitle'], "Name": product["Name"],
                             "LargImageUrl": product["LargImageUrl"], "FullName": product["FullName"],
                             "IsAvailableText": product["IsAvailableText"],
                             "FinalUnitPrice": product["FinalUnitPrice"]})
        return {"Products": products}

    async def run(self) -> None:
        # Run the crawling
        categories = [
            {"CategoryId": 98},
            {"CategoryId": 1},
            {"CategoryId": 24},
            {"CategoryId": 41}
        ]
        # Add task of CategoryId
        tasks = []
        for params in categories:
            tasks.append(self.web_scrape_task(params))
        await asyncio.wait(tasks)